package com.example.tbcdialogexample

import android.app.Dialog
import android.view.Window
import android.view.WindowManager
import androidx.viewbinding.ViewBinding

fun Dialog.setDialog(color: Int,binding: ViewBinding) {

    window!!.setBackgroundDrawableResource(color)
    window!!.requestFeature(Window.FEATURE_NO_TITLE)
    val params = this.window!!.attributes
    params.width = WindowManager.LayoutParams.MATCH_PARENT
    params.height = WindowManager.LayoutParams.WRAP_CONTENT
    setContentView(binding.root)
    this.show()
}