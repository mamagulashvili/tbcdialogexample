package com.example.tbcdialogexample

import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.*
import android.net.NetworkCapabilities.*
import android.os.Build
import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.tbcdialogexample.databinding.DialogNoInternetBinding
import com.example.tbcdialogexample.databinding.FragmentMainBinding

class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        if (hasInternetConnection()) {
            binding.text.visibility = View.VISIBLE
        } else {
            binding.text.visibility = View.GONE
            showDialog("No Internet Connection")
        }
    }

    private fun hasInternetConnection(): Boolean {
            d("INSTANCE", "${MyApp.applicationContext()}")
        val connectionManager = MyApp.applicationContext().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectionManager.activeNetwork ?: return false
            val capabilities =
                connectionManager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(TRANSPORT_WIFI) -> true
                capabilities.hasTransport(TRANSPORT_CELLULAR) -> true
                capabilities.hasTransport(TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectionManager.activeNetworkInfo?.run {
                return when (type) {
                    TYPE_WIFI -> true
                    TYPE_ETHERNET -> true
                    TYPE_MOBILE -> true
                    else -> false
                }
            }
        }
        return false
    }

    private fun showDialog(message: String) {
        val errorDialog = Dialog(requireContext())
        val dialogBinding = DialogNoInternetBinding.inflate(layoutInflater)
        errorDialog.setDialog(android.R.color.transparent,dialogBinding)

        dialogBinding.tvError.text = message
        dialogBinding.btnRetry.setOnClickListener {
            errorDialog.cancel()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}